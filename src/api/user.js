
import request from "../utils/request";


// 登录
export const login = (code) => request.post('/login', {
    principal: code
})
// 轮播
export const img = () => request.get('/indexImgs')
// 最新公告
export const topNoticeList = () => request.get('/shop/notice/topNoticeList')
// 商品列表标题
export const prodList = (id) => request.get(`/prod/tag/prodTagList`+id)
// 商品列表内容
// export const prodTagList = (tagId) => request.get('/prod/prodListByTagId'+tagId)
// 新品推荐
export const newProd = () => request.get('/prod/lastedProdPage?current=1&size=10')
// 限时特惠
export const shopCart = () => request.get('/p/shopCart/prodCount')
// 每日疯抢
export const moreBuy = () => request.get('/prod/moreBuyProdList?current=1&size=10')