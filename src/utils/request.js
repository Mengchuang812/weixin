

class Request {
    constructor({ timeout = 60000, header = {}, basUrl = '' }) {
        // this.methods = ['get', 'post'];
        this.timeout = timeout;
        this.header = header;
        this.basUrl = basUrl
    }
    forMatUrl(url, params) {
        if (typeof params === 'object') {
            params = Object.keys(params).map(key => `${key}=${params[key]}`).join('&')
        }
        // 判断地址后面有没有？号
        if (url.includes('?')) {
            return `${url}${url.endsWith('&') ? '' : '&'}${params}`
        }
        return `${url}?${params}`
    }
    get(url = '', params = {}, config = {}) {
      return this.sendRequest({
            url: this.forMatUrl(url, params),
            method: 'GET',
            config
        })
    }
    post(url = '', data = {}, config = {}) {
       return this.sendRequest({
            url,
            method: 'POST',
            data,
            config
        })
    }
    sendRequest({ config, ...option }) {
        // console.log(config, option);
        const httpConfig = {
            ...option,
            url: `${(/^https?$/).test(option.url) ? "" : this.basUrl}${option.url}`,
            header: {
                ...this.header,
                ...(config.header || {})
            },
            timeout: this.timeout
        }
        return new Promise((resolve, reject) => {
            uni.request({
                ...httpConfig,
                success(res) {
                    resolve(res.data || res)
                },
                fail(error) {
                    reject(error)
                }
            })
        })
    };

}

const userToken = uni.getStorageSync('userinfo')

export default new Request({
    timeout: 10000,
    basUrl: 'https://bjwz.bwie.com/mall4j',
    header: {
        'token': userToken ? `${userToken.token_type}${userToken.access_token}` : ''
    }
})