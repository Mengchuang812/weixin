

export const isH5 = process.env.VUE_APP_PLATFORM === 'h5';
export const isWxin = process.env.VUE_APP_PLATFORM === 'mp-weixin';